const images = [
    'https://cdn-dynmedia-1.microsoft.com/is/image/microsoftcorp/Overview_3-1-1?resMode=sharp2&op_usm=1.5,0.65,15,0&wid=1600&hei=650&qlt=95&fmt=png-alpha&fit=constrain',
    'https://cdn-dynmedia-1.microsoft.com/is/image/microsoftcorp/Overview_3-2-832x624?resMode=sharp2&op_usm=1.5,0.65,15,0&wid=832&hei=624&qlt=95&fmt=png-alpha&fit=constrain',
    'https://cdn-dynmedia-1.microsoft.com/is/image/microsoftcorp/Overview_3-3-832x624?resMode=sharp2&op_usm=1.5,0.65,15,0&wid=832&hei=624&qlt=95&fmt=png-alpha&fit=constrain'
];

function toggleAccordion(index) {
    const sections = document.querySelectorAll('#accordion > div');
    sections.forEach((section, i) => {
        const content = section.querySelector('div');
        const button = section.querySelector('button');
        if (i === index) {
            content.classList.toggle('hidden');
            section.classList.toggle('border-selected');
        } else {
            content.classList.add('hidden');
            section.classList.remove('border-selected');
        }
    });
    // Cambiar la imagen solo si la sección está visible
    const imageElement = document.getElementById('accordion-image');
    if (!sections[index].querySelector('div').classList.contains('hidden')) {
        imageElement.src = images[index];
    } else {
        imageElement.src = 'https://via.placeholder.com/400?text=Selecciona+una+sección';
    }
}

