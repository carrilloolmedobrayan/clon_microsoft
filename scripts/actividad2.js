document.addEventListener("DOMContentLoaded", () => {
    const dropdown = document.querySelector(".relative a");
    const menu = document.querySelector(".relative .absolute");
  
    dropdown.addEventListener("click", (event) => {
      event.preventDefault();
      menu.classList.toggle("hidden");
    });
  });
  