    var autoplayInterval;

    function togglePlay() {
        var button = document.getElementById('playPauseBtn');
        var svg1 = '<svg width="64px" height="64px" viewBox="-24 -24 72.00 72.00" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><path d="M12,1A11,11,0,1,0,23,12,11.013,11.013,0,0,0,12,1Zm0,20a9,9,0,1,1,9-9A9.011,9.011,0,0,1,12,21ZM10,8l6,4-6,4Z"></path></g></svg>';
        var svg2 = '<svg width="64px" height="64px" viewBox="-24 -24 72.00 72.00" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"><g id="Media / Pause_Circle"><path id="Vector" d="M14 9V15M10 9V15M12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21Z" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>';

        if (button.innerHTML === svg1) {
            button.innerHTML = svg2;
            autoplayInterval = setInterval(nextSlide, 5000);
        } else {
            button.innerHTML = svg1;
            clearInterval(autoplayInterval);
        }
    }

    autoplayInterval = setInterval(nextSlide, 5000);

    var currentSlideIndex = 0;

    function showSlide(index) {
        var slides = document.querySelectorAll('#carousel > div');
        if (index >= slides.length) {
            index = 0;
        } else if (index < 0) {
            index = slides.length - 1;
        }

        for (var i = 0; i < slides.length; i++) {
            if (i === index) {
                slides[i].classList.remove('hidden');
            } else {
                slides[i].classList.add('hidden');
            }
        }

        currentSlideIndex = index;
    }

    function prevSlide() {
        showSlide(currentSlideIndex - 1);
    }

    function nextSlide() {
        showSlide(currentSlideIndex + 1);
    }