function activateLink(element) {
    // Remove the active class from all elements
    document.querySelectorAll('.border-blue-900').forEach(el => el.classList.remove('border-blue-900'));
    // Add the active class to the clicked element
    element.classList.add('border-blue-900');
}
window.addEventListener('scroll', function() {
    var section = document.getElementById('miSeccion');
    var rect = section.getBoundingClientRect();
    var topOffset = rect.top;
    
    if (topOffset <= 0) {
        section.classList.add('fixed', 'top-0', 'left-0', 'w-full', 'z-10');
    } else {
        section.classList.remove('fixed', 'top-0', 'left-0', 'w-full', 'z-10');
    }
});
