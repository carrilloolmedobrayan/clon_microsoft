/* const carouselImage = document.getElementById('card');
const controlButtons = document.querySelectorAll('button');

function moveToSlide(slideIndex) {
    const slideWidth = 2000; // Ancho de cada slide
    const offset = slideIndex * slideWidth;
    carouselImage.style.transform = `translateX(-${offset}px)`;

    // Actualizar el estilo de los botones de control
    controlButtons.forEach((btn, index) => {
        if (index === slideIndex) {
            btn.classList.remove('bg-blue-500');
            btn.classList.add('bg-blue-600');
        } else {
            btn.classList.remove('bg-blue-600');
            btn.classList.add('bg-blue-500');
        }
    });
} */

function moveToSlide(index) {
    if(index==0){
        const cardWidth = document.getElementById('card1').offsetWidth; // Ancho de cada tarjeta
        const offset = -index * cardWidth; // Calcula el desplazamiento necesario
        document.getElementById('carousel').style.transform = `translateX(${offset}px)`; // Aplica el desplazamiento
    }
    if(index==1){
        const cardWidth = document.getElementById('card1').offsetWidth; // Ancho de cada tarjeta
        const offset = -index * cardWidth-350; // Calcula el desplazamiento necesario
        document.getElementById('carousel').style.transform = `translateX(${offset}px)`; // Aplica el desplazamiento
    }
    if(index==2){
        const cardWidth = document.getElementById('card2').offsetWidth; // Ancho de cada tarjeta
        const offset = -index * cardWidth-700; // Calcula el desplazamiento necesario
        document.getElementById('carousel').style.transform = `translateX(${offset}px)`; // Aplica el desplazamiento
    }
    if(index==3){
        const cardWidth = document.getElementById('card2').offsetWidth; // Ancho de cada tarjeta
        const offset = -index * cardWidth-1050; // Calcula el desplazamiento necesario
        document.getElementById('carousel').style.transform = `translateX(${offset}px)`; // Aplica el desplazamiento
    }

}